// Setting prototypes - tmp.gpro
/**
 * Find a string in the array matching given regex
 * @param {String} match String of regex to match with string
 * @return {String|null} Returns matching string or null if nothing is found
 */
 Array.prototype.findReg = function (match) {
  let reg = new RegExp(match)

  return this.filter(function (item) {
    return typeof item == 'string' && item.match(reg)
  })
}

/**
 * Removed value from array
 * @param {*} value Value to look for and remove out of array
 * @return {Boolean} Returns true on success (removed value or value not found) and false if the given value is empty
 * @description Removed value from array. Note that this function doesn't stop at the first match!
 */
Array.prototype.removeByValue = function (value) {
  if (isEmpty(value)) return false
  for (let i = 0; i < this.length; i++) {
    const element = this[i];
    if (element == value) this.splice(i, 1)
  }
  return true
}

// Name of the script
const programName = "Blob Receiver"
// Setting module name which will be used by debug handler
const moduleName = "BR"

// Global variables


// Flags - tmp.flags


// Loading modules - tmp.modules
// Load express module
const express = require("express")
console.log("Module 'express' loaded as 'express'.")
// Load fs module
const fs = require("fs")
console.log("Module 'fs' loaded as 'fs'.")
// Load https module
const http = require("http")
console.log("Module 'http' loaded as 'http'.")
// Load https module
const https = require("https")
console.log("Module 'https' loaded as 'https'.")
// Load sharp module
const sharp = require("sharp")
console.log("Module 'sharp' loaded as 'sharp'.")
// Load sharp module
const iso = require("image-size")
console.log("Module 'image-size' loaded as 'imgSizeOf'.")
// Load https module
/* const bodyParser = require('body-parser')
console.log("Module 'body-parser' loaded as 'bodyParser'.") */

// Prototypes of loaded modules - tmp.proto.modules
function imgSizeOf(filePath) {
  return new Promise((resolve, reject) => {
    iso(filePath, (err, dim) => {
      if (err) reject(err)
      else resolve(dim)
    })
  })
}

/**
 * @param {String} path The path to a file you want to 'touch'
 * @returns {Promise} Returns a Promise. On resolve it returns true if the file got touched, reject returns an error if any
 * @requires fs
 * @desc Basiclly the Linux `touch` command made with `fs`
 */
const touch = function (path) {
  return new Promise(function (resolve, reject) {
    fs.open(path, 'a', function (err, fnd) {
      if (err) reject(err)
      fs.close(fnd, function (err2) {
        if (err2) reject(err2)
        resolve(true)
      })
    })
  })
}

/**
 * @abstract
 * @param {!String} path The path to a file you want to write the `data` to
 * @param {*} data The data you want to write to the file
 * @param {?Object} [options={}] An object with `fs.writeFile()` options
 * @returns {!Promise} Returns a Promise. On resolve it returns true if the data got written succesfully, reject returns a error if any
 * @see fs.writeFile()
 * @see fs.writeFileSync()
 * @requires fs
 * @desc Same as `fs.writeFileSync()` but with error handling support
 */
const writeFileSync = function (path, data, options = {}) {
  return new Promise(function (resolve, reject) {
    fs.writeFile(path, data, options, function (err) {
      if (err) reject(err)
      resolve(true)
    })
  })
}

/**
 * @abstract
 * @param {!String} path The path to a file you want to read
 * @param {?Object} [options={}] An object with `fs.readFile()` options
 * @returns {!Promise} Returns a Promise. On resolve it returns the read data, reject returns a error if any
 * @see fs.readFile()
 * @see fs.readFileSync()
 * @requires fs
 * @desc Same as `fs.readFileSync()` but with error handling support
 */
const readFileSync = (path, options) => new Promise((resolve, reject) => fs.readFile(path, options, (err, dataBuffer) => {
  if (err) reject(err)
  resolve(dataBuffer.toString("utf8"))
}))

// Classes
class Request {
  static async request(options) {
    return new Promise((resolve, reject) => {
      if (typeof options.data === "object") options.data = JSON.stringify(options.data)
      else if (typeof options.data !== "string" && !isEmpty(options.data)) options.data = options.data.toString()
      let request = https.request(options, (res) => {
        let chunks = []
        res.on('data', (chunk) => chunks.push(chunk))
        res.on('end', () => {
          let body = Buffer.concat(chunks).toString()
          if (res.headers['content-type'].includes("application/json")) body = JSON.parse(body);
          if (res.statusCode != 200) reject({
            error: res.statusMessage,
            "status": res.statusCode,
            res: body
          })
          else resolve({
            body,
            "headers": res.headers,
            "status": res.statusCode
          })
        })
      })
      request.on("error", (e) => reject({
        error: e,
        "status": 500,
        res: null
      }))
      if (!isEmpty(options.data)) request.write(options.data)
      request.end()
    })
  }

  static async get(uri, headers = {}) {
    return new Promise((resolve, reject) => {
      let request = https.request(uri, {
        "method": "GET",
        headers
      }, (res) => {
        let chunks = []
        res.on('data', (chunk) => chunks.push(chunk))
        res.on('end', () => {
          let body = Buffer.concat(chunks).toString()
          if (res.headers['content-type'].includes("application/json")) body = JSON.parse(body);
          if (res.statusCode != 200) reject({
            error: res.statusCode,
            res: body
          })
          else resolve(body)
        })
      })
      request.on("error", (e) => reject({
        error: e,
        res: {
          statusCode: 500
        }
      }))
      request.end()
    })
  }

  static async post(uri, headers = {}, body, contentType = "text/plain") {
    return new Promise((resolve, reject) => {
      if (typeof body === "object") {
        if (contentType === "application/x-www-form-urlencoded") body = toUrlEncoded(body)
        else {
          body = JSON.stringify(body)
          contentType = "application/json"
        }
      } else if (typeof body !== "string" && !isEmpty(body)) body = body.toString()
      else body = ""
      let request = https.request(uri, {
        "method": "POST",
        "headers": {
          "Content-Type": contentType,
          ...headers
        }
      }, (res) => {
        let chunks = []
        res.on('data', (chunk) => chunks.push(chunk))
        res.on('end', () => {
          let body = Buffer.concat(chunks).toString()
          if (res.headers['content-type'].includes("application/json")) body = JSON.parse(body);
          if (res.statusCode != 200) reject({
            error: res.statusCode,
            res: body
          })
          else resolve({
            body,
            "headers": res.headers
          })
        })
      })
      request.on("error", (e) => reject({
        error: e,
        res: {
          statusCode: 500
        }
      }))
      request.write(body)
      request.end()
    })
  }

  static createArgumentString(args = {}) {
    let argStr = ""
    let amount = 0
    for (const key in args) {
      if (Object.hasOwnProperty.call(args, key)) {
        const arg = args[key];
        if (isEmpty(arg)) continue
        if (amount === 0) argStr += `?`
        else argStr += `&`
        argStr += `${key}=${arg}`
        amount++
      }
    }
    return argStr
  }
}

// Loading config variables
var expressConfig
if (!fs.existsSync("./config.js")) {
  console.error("Config file couldn't be found. Loading default from hard-code...")
  expressConfig = {
    "port": 1234,
    "host": "127.0.0.1"
  }
  console.error("WARNING! Loading from hard-code disables the ability to login and basicly makes this program unusable.\nPlease create a config file!!")
} else {
  expressConfig = require("./config.js")
  console.log("Config loaded from file!")
}
const prettyPort = (expressConfig.port === 80 || expressConfig.port === 443) ? "" : ":" + expressConfig.port

// Config location/files check

// General functions - tmp.gfunc
const Sleep = (ms) => new Promise((resolve, reject) => setTimeout(() => resolve(), ms))

/**
 * My famous (not really) isEmpty function. It checks if a given variable is empty or not
 * @param {*} variable Anything you want to know if it's empty or not
 * @returns {Boolean} Returns true on empty and false on not empty
 */
function isEmpty(variable) {
  if (variable == null || variable == undefined || Number.isNaN(variable)) return true
  else if (typeof variable == "string" && variable == "") return true
  else if (typeof variable == "object") {
    if (Array.isArray(variable) && variable.length == 0) return true
    else if (Object.keys(variable).length == 0) return true
  }
  return false
}

/**
 * My second famous (not really) isEmptyAny function. It checks if any variable in a given array is empty or not
 * @param {Array} array An array with elements you want to know if any of them are empty
 * @returns {Boolean} Returns true on any of them being empty and false on none of them being empty
 */
function isEmptyAny(array) {
  for (let i = 0; i < array.length; i++) {
    if (isEmpty(array[i])) {
      return true
    }
  }
  return false
}

// Program's helper functions
function argumentBuilder(argumentObj, get, post) {
  if (isEmpty(argumentObj)) return []
  let arguments = []
  for (const method in argumentObj) {
    if (argumentObj.hasOwnProperty(method)) {
      const keyArray = argumentObj[method];
      if (method === "GET") {
        if (keyArray === "*") {
          arguments.push(get)
        } else if (Array.isArray(keyArray)) {
          for (let j = 0; j < keyArray.length; j++) {
            const key = keyArray[j];
            arguments.push(get[key])
          }
        } else continue
      } else if (method === "POST") {
        if (keyArray === "*") {
          arguments.push(post)
        } else if (Array.isArray(keyArray)) {
          for (let j = 0; j < keyArray.length; j++) {
            const key = keyArray[j];
            arguments.push(post[key])
          }
        } else continue
      } else continue
    }
  }
  return arguments
}

function resAPIHandling(res, {success = null, data = null, error = null}, statusCode = 200) {
  if (success === undefined) success = null
  if (data === undefined) data = null
  if (error === undefined) error = null
  res.statusCode = statusCode
  res.setHeader('Content-Type', 'application/json')
  res.json({success, data, error})
}

function randomStringGenerator(length) {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function randomIntFromInterval(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min)
}

var authenticator = () => {
  console.log("STANDALONE MORE")
  return true
}

// Program specific variables
const app = express()

// app.use(bodyParser.json());
app.use(express.json({extended: true, limit: '50mb'}));
app.use(express.urlencoded({extended: true}));

app.set('view engine', 'ejs');

// The npm page of express-session didn't tell me I can't just regenerate keys at every start so fuck them here we go! :P

// Program's (handler) functions
app.post("/save_blob", async (req, res) => {
  let blob = req.body.blob
  let fileName = req.body.fileName
  if (isEmpty(blob) || isEmpty(fileName)) {
    console.dir({"body": req.body})
    return resAPIHandling(res, {"success": false, "error": "Missing parameters"})
  }
  // return resAPIHandling(res, {"success": false, "error": "debug"})
  let extRE = new RegExp(/^data:image\/(png|jpeg|jpg);base64,/).exec(blob)
  let ext = "png"
  if (!isEmpty(extRE)) ext = extRE[1]
  let base64Data = blob.replace(/^data:image\/(png|jpeg|jpg);base64,/, "");
  let filePath = `./output/${fileName}.${ext}`
  fs.writeFile(filePath, base64Data, {
    "encoding": "base64"
  }, async (err) => {
    if (err) {
      console.error("An error occurred while creating a file.", err)
      return resAPIHandling(res, {"success": false, "error": err})
    }
    else {
      console.debug("Created a file!", {"file": filePath})
      if (fileName.includes("mugshot")) {
        let dimensions = await imgSizeOf(filePath)
        sharp(filePath).extract({"width": parseInt(dimensions.width / 11), "height": parseInt(dimensions.height / 6), "left": 0, "top": 0}).toFile(`./output/${fileName}_cropped.${ext}`).then(() => console.debug("Cropped the file!", {"file": `./output/${fileName}_cropped.${ext}`}))
      }
      return resAPIHandling(res, {"success": true})
    }
  })
})

app.post("/post_test", (req, res) => {
  return res.json({
    "GET": req.query,
    "POST": req.body
  })
})

app.get("/test_auth", (req, res) => {
  res.json({"result": authenticator()})
})

app.use((err, req, res, next) => {
  console.error(err.message, err.stack)
  resAPIHandling(res, {"success": false, "error": 500})
})

process.on('uncaughtException', (err) => console.error(err.message, err.stack))

process.on('unhandledRejection', (reason, p) => console.error("unhandledRejection: ", {reason, p}))

// Code execution
var server
async function bootUp() {
  try {
    // If this server has a self-signed certificated ignore SSC's
    process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = (expressConfig.certificateIsSelfSigned) ? 0 : 1
    // HTTP Server
    let httpsCredentials = {}
    let httpClass = http
    if (fs.existsSync(expressConfig.keyFile) && fs.existsSync(expressConfig.certFile)) {
      httpsCredentials = {
        key: await readFileSync(expressConfig.keyFile),
        cert: await readFileSync(expressConfig.certFile)
      }
      httpClass = https
    }
    else console.error("Missing server.key and/or server.cert file to create a HTTPS server. Running without SSL. (You can easily create self-signed certificate by running `npm run gen-ssl`)")
    // Create necessary folders
    if (!fs.existsSync("./output")) fs.mkdirSync("./output")
    // Once all is done catch any other endpoints that aren't catched
    app.get("*", (req, res) => res.json({"error": 404}))
    app.post("*", (req, res) => res.json({"error": 404}))
    server = httpClass.createServer(httpsCredentials, app).listen(expressConfig.port, expressConfig.host)
    console.log(`HTTP(S) server running on ${expressConfig.host}:${expressConfig.port}. Waiting for requests...`)
  } catch (e) {
    let msg = e
    let stacktrace
    if (!!e.message) {
      msg = e.message
      stacktrace = e.stack
    }
    console.error("Error: " + msg, stacktrace)
    process.exit(1)
  }
}
bootUp()