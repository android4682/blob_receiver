-- Resource Metadata
fx_version 'bodacious'
games { 'gta5' }

author 'Andrew de Jong'
description 'Blob receiver'
version '1.0.0'

-- What to run
server_scripts {
    "server.js"
}